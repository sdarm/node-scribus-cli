FROM node:10-buster

RUN echo deb http://ftp.debian.org/debian/ unstable main contrib non-free > /etc/apt/sources.list && \
    echo deb http://ftp.de.debian.org/debian buster main >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y git xvfb xauth ttf-mscorefonts-installer
RUN apt-get -y -t unstable install scribus

ADD https://raw.githubusercontent.com/aoloe/scribus-script-repository/master/to-pdf/to-pdf.py /usr/bin

